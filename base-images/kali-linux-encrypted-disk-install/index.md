---
title: Encrypted Disk Install
description:
icon:
date: 2020-05-28
type: archived
weight: 55
author: ["gamb1t",]
tags: ["",]
keywords: ["",]
og_description:
---

This has been moved to the [installation](https://www.kali.org/docs/installation/) section. To access this doc specifically, please follow [this link](https://www.kali.org/docs/installation/kali-linux-encrypted-disk-install/).