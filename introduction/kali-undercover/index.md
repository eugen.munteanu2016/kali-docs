---
title: Kali Undercover
description:
icon:
date: 2020-06-29
type: post
weight: 40
author: ["theGorkha",]
tags: ["",]
keywords: [""]
og_description:
---

## Kali Undercover

**Kali Undercover** is a set of scripts that changes the look and feel of your Kali Linux desktop environment to **Windows 10** desktop environment, like *magic*.

It was released with [**Kali Linux 2019.4**](https://www.kali.org/news/kali-linux-2019-4-release/) with an important concept in mind, *to hide in plain sight*.

## Getting "undercover"

Switching to undercover mode is pretty straight-forward, just run the following command inside command-line:

```console

kali@kali:~$ kali-undercover

```

or, you can also look for **"Kali Undercover Mode"** from menu of your desktop and launch it.

**Swoosh!** Now, you are completely(*almost*) invisible. The script will turn your desktop environment's look and feel similar to that of Windows 10.  

## Reverting Back

Now, to **revert** back into your previous desktop settings just re-enter the previous command:

```console

kali@kali:~$ kali-undercover

```

**Ta-da!** Welcome back! Now, all your desktop settings should be restored to .

## Purpose of Undercover Mode in Kali Linux

The main purpose of introducing **Kali Undercover** mode is **to prevent any unnecessary attention** while using Kali Linux in *public*.

Let's imagine a scenario: you are *pentesting* your client (*ethically*) and you are in their office or reception doing reconnaissance or something that involves the use of **Kali Linux**.

What if someone from your client's office or some random on-looker spot your desktop environment/wallpaper of Kali Linux and he/she might think you are doing something mischievous even though you are doing it *ethically* and warn the authority. All the hard work that you did from the beginning to become stealthy will go in vain. That also because of what? A **wallpaper**!  
For a client who requested you to become stealthy, this is not what you would want.

Hence, to prevent any kind of unwanted attention from public its better to go **"Undercover!"**.
